# README #
* Relatively simple program for generating mazes of any specified dimension
* Has no dependencies, should compile with javac with no special options

I created this program to generate test data for a maze solver I created as part of an introductory Java course at university.
For that reason I was forced to implement data structures like Sets and expendable arrays from scratch, in stead of using the standard implementations. 