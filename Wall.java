/* A class for representing a Wall in a maze
 * */
public class Wall
{
  // The cells on side a and side b
  private final Cell sideA;
  private final Cell sideB;


  public Wall(Cell sideA_value, Cell sideB_value)
  {
    sideA = sideA_value;
    sideB = sideB_value;
  }


  // A wall is proper if it separates two paths
  // so you can't get from one cell to the other
  public boolean isProper()
  {
    //System.out.println("Is Connected: " + sideA.isConnected(sideB));
    return ! sideA.isConnected(sideB);
  }

  public boolean equals(Wall otherWall)
  {
    return sideA == otherWall.sideA && sideB == otherWall.sideB;
  }

  // Remove a wall, therefore connecting the two sides
  public void tearDown()
  {
    sideA.connect(sideB);
  }

}
