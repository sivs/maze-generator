// A collection of Walls
public class SetOfWalls 
{
  private final int INITIAL_ARRAY_SIZE = 10;
  private final int ARRAY_RESIZE_FACTOR = 2;
  private int noOfItemsInSet = 0;

  private Wall[] wallsArray;

  public SetOfWalls()
  {
    wallsArray = new Wall[INITIAL_ARRAY_SIZE];
  }

  private int getArrayIndex(Wall anItem)
  {
    int arrayIndex = 0;
    while(arrayIndex < (noOfItemsInSet-1) &&
        wallsArray[arrayIndex] != anItem)
      arrayIndex++;

    //System.out.println("Index:" + arrayIndex);
    //System.out.println("noOfItemsInSet: " + noOfItemsInSet);
    //System.out.println("Array length: " + wallsArray.length);
    // In case itemToDrop is not in the set
    if(wallsArray[arrayIndex] != anItem) return -1;
    else return arrayIndex;
  }

  // Remove an item from the set
  // returns false if item is not in set
  public boolean drop(Wall itemToDrop)
  {
    int arrayIndex = getArrayIndex(itemToDrop);

    // In case itemToDrop is not in the set
    if(arrayIndex == -1) return false;

    int lastItemIndex = noOfItemsInSet - 1;
    wallsArray[arrayIndex] = wallsArray[lastItemIndex];

    // Decrease number of elements in the set
    // effectivly deleting the reference
    // to the last item
    noOfItemsInSet--;

    return true;
  }

  public void add(Wall itemToAdd)
  {
    // If item already is in set
    //System.out.println("ArrayIndex: " + getArrayIndex(itemToAdd));
    if(getArrayIndex(itemToAdd) != -1) return;

    if(noOfItemsInSet == wallsArray.length)
    {
      Wall[] biggerArray = new Wall[wallsArray.length * ARRAY_RESIZE_FACTOR];
      // Copy over items
      for(int i = 0; i < noOfItemsInSet; i++)
        biggerArray[i] = wallsArray[i];

      wallsArray = biggerArray;
    }

    wallsArray[noOfItemsInSet] = itemToAdd;

    noOfItemsInSet++;
  }

  // Return a random element from the set
  public Wall getRandomItem()
  {
    // Will give random array index between 0 and noOfItemsInSet - 1
    int randomArrayIndex = (int) (Math.random() * noOfItemsInSet);
    //System.out.println("randomArrayIndex: " + randomArrayIndex);

    return wallsArray[randomArrayIndex];
  }

  public int getSize()
  {
    return noOfItemsInSet;
  }

  // Remove walls that don't separate to partitions 
  public void dropNonProperWalls()
  {
    for(int i = 0; i < noOfItemsInSet; i++)
      if(! wallsArray[i].isProper() )
        drop(wallsArray[i]);
  }

  public void addSet(SetOfWalls otherSet)
  {
    for(int i = 0; i < otherSet.getSize(); i++)
      this.add(otherSet.wallsArray[i]);
  }

  public SetOfWalls getNonProper()
  {
    SetOfWalls nonProperWalls = new SetOfWalls();

    for(int i = 0; i < noOfItemsInSet; i++)
    {
      if(! wallsArray[i].isProper())
        nonProperWalls.add(wallsArray[i]);
    }

    return nonProperWalls;
  }

  public boolean contains(Wall theWall)
  {
    for(int i = 0; i < noOfItemsInSet; i++)
      if(wallsArray[i].equals(theWall))
        return true;

    return false;
  } // sharesWall
}
