/* A class for representing a Maze
 *
 * */
public class Maze
{
  private SetOfWalls separatingWalls;
  private SetOfWalls nonSeparatingWalls;

  private Cell[][] cells;

  private int width;
  private int height;

  private final String HEDGE_REP = "#";
  private final String SPACE_REP = " ";

  private final String NLS = System.getProperty("line.separator");

  public Maze(int width_val, int height_val)
  {
    separatingWalls = new SetOfWalls();
    nonSeparatingWalls = new SetOfWalls();

    width = width_val;
    height = height_val;

    generateMaze();
  }

  private void generateMaze()
  {
    // Create the cells and store them in a 2D array
    cells = new Cell[width][height];


    // Create the interior walls
    for(int column = 0; column < width; column++)
      for(int row = 0; row < height; row++)
      {
        Cell cell = cells[column][row] = new Cell();

        if(row > 0) 
          separatingWalls.add(new Wall(cell, cells[column][row-1]));

        if(column > 0) 
          separatingWalls.add(new Wall(cell, cells[column-1][row])); 
      }

    // As long as all cells are not connected
    while(separatingWalls.getSize() > 0)
    {
      Wall wallToRemove = separatingWalls.getRandomItem();

      // Join connect sides
      wallToRemove.tearDown();

      // Delete wall
      separatingWalls.drop(wallToRemove);

      // Add non proper walls to nonSeparatingWalls
      nonSeparatingWalls.addSet(separatingWalls.getNonProper());

      // Remove non proper walls from separatingWalls
      separatingWalls.dropNonProperWalls();
    }
  }

  private String getSolidRow()
  {
    String result = "";
    for(int col = 0; col < 2*width + 1; col++) result += HEDGE_REP;
    return result + NLS;
  }

  public String toString()
  {
    int printWidth = 2*width - 1;
    int printHeight = 2*height -1;

    String result = "";
    // Add a row of # on the top
    result += getSolidRow();

    for(int row = 0; row < printHeight; row++)
    {
      result += HEDGE_REP;
      for(int col = 0; col < printWidth; col++)
      {
        String cellValue;
        if(row % 2 == 0 && col % 2 == 0) cellValue = SPACE_REP;
        else if (row % 2 == 1 && col % 2 == 1) cellValue = HEDGE_REP;
        else if (row % 2 == 0)
        {
          Cell toTheLeft = cells[(col)/2][(row+1)/2];
          Cell toTheRight = cells[((col+2)/2)][(row+1)/2];

          cellValue = 
            nonSeparatingWalls.contains(new Wall(toTheRight, toTheLeft))
                          ? HEDGE_REP : SPACE_REP;
        }
        else 
        {
          Cell cellOver   = cells[(col+1)/2][(row+2)/2];
          Cell cellUnder  = cells[(col+1)/2][(row)/2];

          cellValue = 
            nonSeparatingWalls.contains(new Wall(cellOver, cellUnder))
                            ? HEDGE_REP : SPACE_REP;
        }

        result += cellValue;
      }
      result += HEDGE_REP + String.format("%n");
    }

    // Add a row of # to on the bottom
    result += getSolidRow(); 

    return result;
  } // toString
}
