/* A class for representing a cell in a maze
 *
 * */
public class Cell
{
  private Cell parent;
  public Cell()
  {
    parent = this;
  }

  private Cell getParent()
  {
    if(parent == this) return this;
    else return parent.getParent();
  }
  public boolean isConnected(Cell otherCell)
  {
    return getParent() == otherCell.getParent();
  } // compareSet

  public void connect(Cell otherCell)
  {
    getParent().parent = otherCell.getParent();
  } // join
}
